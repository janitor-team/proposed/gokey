Source: gokey
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Benjamin Drung <benjamin.drung@cloud.ionos.com>,
           Anthony Fok <foka@debian.org>
Section: utils
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper (>= 11),
               dh-golang,
               golang-any,
               golang-golang-x-crypto-dev,
               pandoc
Standards-Version: 4.4.0
Vcs-Browser: https://salsa.debian.org/go-team/packages/gokey
Vcs-Git: https://salsa.debian.org/go-team/packages/gokey.git
Homepage: https://github.com/cloudflare/gokey
XS-Go-Import-Path: github.com/cloudflare/gokey

Package: gokey
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Built-Using: ${misc:Built-Using}
Description: simple vaultless password manager in Go
 gokey is a password manager, which does not require a password vault. Instead
 of storing your passwords in a vault it derives your password on the fly from
 your master password and supplied realm string (for example, resource URL).
 This way you do not have to manage, backup or sync your password vault (or
 trust its management to a third party) as your passwords are available
 immediately anywhere.

Package: golang-github-cloudflare-gokey-dev
Architecture: all
Section: devel
Depends: golang-golang-x-crypto-dev,
         ${misc:Depends},
         ${shlibs:Depends}
Description: simple vaultless password manager in Go - dev package
 gokey is a password manager, which does not require a password vault. Instead
 of storing your passwords in a vault it derives your password on the fly from
 your master password and supplied realm string (for example, resource URL).
 This way you do not have to manage, backup or sync your password vault (or
 trust its management to a third party) as your passwords are available
 immediately anywhere.
 .
 This is the development package.
